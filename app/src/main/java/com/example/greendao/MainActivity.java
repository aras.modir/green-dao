package com.example.greendao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Button button;
    private EditText editText;
    private ArrayList<User> users;
    private UserAdapter adapter;
    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        app = new App();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        users = new ArrayList<User>(((App) getApplication()).getDaoSession().getUserDao().loadAll());
        adapter = new UserAdapter(this, users);
        recyclerView.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.setId(null);
                user.setUsername(editText.getText().toString());
                ((App) getApplication()).getDaoSession().getUserDao().insert(user);
                users.add(user);
                adapter.notifyDataSetChanged();
            }
        });


    }

    private void bind() {
        recyclerView = findViewById(R.id.recycle_view);
        button = findViewById(R.id.button);
        editText = findViewById(R.id.edit_text_name);
    }
}
